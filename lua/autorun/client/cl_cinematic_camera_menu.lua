CreateClientConVar("gmod_cinematic_camera_sounds", "0", true, true)
CreateClientConVar("gmod_cinematic_camera_additional_shakiness", "0", true, true)
CreateClientConVar("gmod_cinematic_camera_letterbox", "0", true, true)
CreateClientConVar("gmod_cinematic_camera_noise", "0", true, true)
CreateClientConVar("gmod_cinematic_camera_grain", "0", true, true)
CreateClientConVar("gmod_cinematic_camera_vignette", "0", true, true)
CreateClientConVar("gmod_cinematic_camera_color_correction", "0", true, true)
CreateClientConVar("gmod_cinematic_camera_color_correction_style", "a", true, true)
CreateClientConVar("gmod_cinematic_camera_overlay", "0", true, true)
CreateClientConVar("gmod_cinematic_camera_shake", "2", true, true)
CreateClientConVar("gmod_cinematic_camera_defaultfov", "75", true, true)
CreateClientConVar("gmod_cinematic_camera_zoom1fov", "54.4", true, true)
CreateClientConVar("gmod_cinematic_camera_zoom2fov", "39.6", true, true)
CreateClientConVar("gmod_cinematic_camera_zoomspeed", "0.275", true, true)

local function CinematicCamera_Menu( Panel )

	local logo = vgui.Create( "DImage" );
	logo:SetImage( "vgui/cc_logo" );
	logo:SetSize( 300, 150 );

	Panel:AddPanel( logo );
	Panel:AddControl( "Label", {Text = "Server Settings"})
	Panel:AddControl( "CheckBox", { Label = "Give Cinematic Camera on spawn", Command = "gmod_give_cinematic_camera_on_spawn" } )
	Panel:AddControl( "Label", {Text = "Client Settings"})
	Panel:AddControl( "CheckBox", { Label = "Camcorder zoom sound", Command = "gmod_cinematic_camera_sounds" } )
	Panel:AddControl( "CheckBox", { Label = "Additional camera shakiness", Command = "gmod_cinematic_camera_additional_shakiness" } )
	Panel:AddControl( "CheckBox", { Label = "Letterbox", Command = "gmod_cinematic_camera_letterbox" } )
	Panel:AddControl( "CheckBox", { Label = "Film grain", Command = "gmod_cinematic_camera_grain" } )
	Panel:AddControl( "CheckBox", { Label = "Vignette", Command = "gmod_cinematic_camera_vignette" } )
	Panel:AddControl( "CheckBox", { Label = "Color Filter", Command = "gmod_cinematic_camera_color_correction" } )
	Panel:AddControl( "ComboBox", {
		Label = "Filter Style",
		Options = {
			["Cold Climatica"]		= { gmod_cinematic_camera_color_correction_style = "a" },
			["Contrasta"]		= { gmod_cinematic_camera_color_correction_style = "b" },
			["Greenity"]		= { gmod_cinematic_camera_color_correction_style = "c" },
			["Warmer Bros"]		= { gmod_cinematic_camera_color_correction_style = "d" },
			["Sadness"]		= { gmod_cinematic_camera_color_correction_style = "e" },
			["Darka"]		= { gmod_cinematic_camera_color_correction_style = "f" },
			["Blockbuster"]		= { gmod_cinematic_camera_color_correction_style = "g" },
			["Field Of Battle"]		= { gmod_cinematic_camera_color_correction_style = "h" },
			["Summer"]		= { gmod_cinematic_camera_color_correction_style = "i" },
			["Calm"]		= { gmod_cinematic_camera_color_correction_style = "j" },
			["Rainmaker"]		= { gmod_cinematic_camera_color_correction_style = "k" },
		}
	})
	Panel:AddControl( "CheckBox", { Label = "Camcorder video noise", Command = "gmod_cinematic_camera_noise" } )
	Panel:AddControl( "CheckBox", { Label = "Camcorder overlay", Command = "gmod_cinematic_camera_overlay" } )
	Params = {}
	Params["Label"] = "Shakiness amount"
	Params["Command"] = "gmod_cinematic_camera_shake"
	Params["Type"] = "Integer"
	Params["Min"] = "0"
	Params["Max"] = "20"
	Panel:AddControl( "Slider", Params)
	
	Params["Label"] = "Zoom Speed"
	Params["Command"] = "gmod_cinematic_camera_zoomspeed"
	Params["Type"] = "Float"
	Params["Min"] = "0.1"
	Params["Max"] = "10"
	Panel:AddControl( "Slider", Params)
	
	Panel:AddControl( "ComboBox", {
		Label = "Default Lens",
		Options = {
			["Original FOV"]		= { gmod_cinematic_camera_defaultfov = "75" },
			["28mm"]		= { gmod_cinematic_camera_defaultfov = "65.5" },
			["35mm"]		= { gmod_cinematic_camera_defaultfov = "54.4" },
			["50mm"]		= { gmod_cinematic_camera_defaultfov = "39.6" },
			["85mm"]		= { gmod_cinematic_camera_defaultfov = "23.9" },
			["105mm"]		= { gmod_cinematic_camera_defaultfov = "19.5" },
			["135mm"]		= { gmod_cinematic_camera_defaultfov = "15.2" },
		}
	})
	Panel:AddControl( "ComboBox", {
		Label = "First Zoom Lens",
		Options = {
			["28mm"]		= { gmod_cinematic_camera_zoom1fov = "65.5" },
			["35mm"]		= { gmod_cinematic_camera_zoom1fov = "54.4" },
			["50mm"]		= { gmod_cinematic_camera_zoom1fov = "39.6" },
			["85mm"]		= { gmod_cinematic_camera_zoom1fov = "23.9" },
			["105mm"]		= { gmod_cinematic_camera_zoom1fov = "19.5" },
			["135mm"]		= { gmod_cinematic_camera_zoom1fov = "15.2" },
		}
	})
	Panel:AddControl( "ComboBox", {
		Label = "Second Zoom Lens",
		Options = {
			["28mm"]		= { gmod_cinematic_camera_zoom2fov = "65.5" },
			["35mm"]		= { gmod_cinematic_camera_zoom2fov = "54.4" },
			["50mm"]		= { gmod_cinematic_camera_zoom2fov = "39.6" },
			["85mm"]		= { gmod_cinematic_camera_zoom2fov = "23.9" },
			["105mm"]		= { gmod_cinematic_camera_zoom2fov = "19.5" },
			["135mm"]		= { gmod_cinematic_camera_zoom2fov = "15.2" },
		}
	})
	Panel:AddControl( "Label", {Text = "Cinematic Camera by Patrick Hunt"})
	Panel:AddControl( "Label", {Text = "Original idea by Empty Shadow"})
end

local function LoadMenu()
	spawnmenu.AddToolMenuOption("Options", "Player", "Camera", "Cinematic camera Options", "", "", CinematicCamera_Menu)
end
hook.Add( "PopulateToolMenu", "Cinematic Camera Load Menu", LoadMenu )