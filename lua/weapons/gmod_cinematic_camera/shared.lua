SWEP.Author			= "Patrick Hunt"
SWEP.Contact		= "patrick07hunt@gmail.com"
SWEP.Purpose		= "This is NOT a recording tool!"
SWEP.Instructions	= "Use secondary attack to zoom in and zoom out. More options are available in the Options/Player section."

SWEP.HoldType		= "rpg"

SWEP.ViewModel		= ""
SWEP.WorldModel		= "models/weapons/w_camera.mdl"

SWEP.Primary.ClipSize	= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic	= false
SWEP.Primary.Ammo	= "none"

SWEP.Secondary.ClipSize	= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo	= "none"

function SWEP:Initialize()
	self:SetWeaponHoldType(self.HoldType)
	ZoomLevel = 0
end

function SWEP:Precache()
	util.PrecacheModel( SWEP.WorldModel )
	util.PrecacheSound( "cc/zoom-in1.wav" )
	util.PrecacheSound( "cc/zoom-in2.wav" )
	util.PrecacheSound( "cc/zoom-out.wav" )
end

function SWEP:Reload()
	local defaultlens = self.Owner:GetInfo("gmod_cinematic_camera_defaultfov")
	self.Owner:SetFOV( defaultlens, 0.25 )
	ZoomLevel = 0
	return true
end

function SWEP:PrimaryAttack()
   return false
end

function SWEP:SecondaryAttack()
	local pitcher = math.random( 90, 105 )
	local defaultlens = self.Owner:GetInfo("gmod_cinematic_camera_defaultfov")
	local zoom1lens = self.Owner:GetInfo("gmod_cinematic_camera_zoom1fov")
	local zoom2lens = self.Owner:GetInfo("gmod_cinematic_camera_zoom2fov")
	local zoomspeed = self.Owner:GetInfo("gmod_cinematic_camera_zoomspeed")
	if(ZoomLevel == 0) then
 		if(SERVER) then
			if GetConVar("gmod_cinematic_camera_sounds"):GetBool() == true then
				self.Weapon:EmitSound( "cc/zoom-in1.wav", 35, pitcher, 1, CHAN_WEAPON )
			end
			self.Owner:SetFOV( zoom1lens, zoomspeed )
		end
		ZoomLevel = 1
	else if(ZoomLevel == 1) then
 		if(SERVER) then
			if GetConVar("gmod_cinematic_camera_sounds"):GetBool() == true then
				self.Weapon:EmitSound( "cc/zoom-in2.wav", 35, pitcher, 1, CHAN_WEAPON )
			end
			self.Owner:SetFOV( zoom2lens, zoomspeed )
		end
		ZoomLevel = 2
 	else
		if(SERVER) then
			if GetConVar("gmod_cinematic_camera_sounds"):GetBool() == true then
				self.Weapon:EmitSound( "cc/zoom-out.wav", 35, pitcher, 1, CHAN_WEAPON )
			end
			self.Owner:SetFOV( defaultlens, zoomspeed )
		end
		ZoomLevel = 0
 		end
	end
end

function SWEP:Think()
	local amount = self.Owner:GetInfo("gmod_cinematic_camera_shake")
	if self.Owner:IsBot() then local amount = 1 else
	self.Owner:ViewPunch( Angle( math.Rand(-amount*0.02,amount*0.02), math.Rand(-amount*0.02,amount*0.02), math.Rand(-amount*0.02,amount*0.02)  ) )
	end
	if GetConVar("gmod_cinematic_camera_additional_shakiness"):GetBool() == true then
	
-- the following code belongs to Empty Shadow

		if self.Owner:KeyDown(IN_FORWARD) then
			self.Owner:ViewPunch( Angle( math.Rand(0,0.2), math.Rand(-0.2,0.2), 0 ) )
		if self.Owner:KeyDown(IN_SPEED) then
			self.Owner:ViewPunch( Angle( math.Rand(0,1.5), math.Rand(-0.2,0.2), math.Rand(-0.2,0.2) ) )
			end
		end

		if self.Owner:KeyDown(IN_MOVELEFT) then
			self.Owner:ViewPunch( Angle( math.Rand(0,0.2), math.Rand(-0.2,0.2), math.Rand(0,-0.3) ) )
		if self.Owner:KeyDown(IN_SPEED) then
			self.Owner:ViewPunch( Angle( math.Rand(0,0.5), math.Rand(-0.2,0.2), math.Rand(0,-0.6) ) )
			end
		end

		if self.Owner:KeyDown(IN_MOVERIGHT) then
			self.Owner:ViewPunch( Angle( math.Rand(0,0.2), math.Rand(-0.2,0.2), math.Rand(0,0.3) ) )
		if self.Owner:KeyDown(IN_SPEED) then
			self.Owner:ViewPunch( Angle( math.Rand(0,0.5), math.Rand(-0.2,0.2), math.Rand(0,0.6) ) )
			end
		end

		if self.Owner:KeyDown(IN_BACK) then
			self.Owner:ViewPunch( Angle( math.Rand(-0.2,0.2), math.Rand(-0.2,0.2), 0 ) )
		if self.Owner:KeyDown(IN_SPEED) then
			self.Owner:ViewPunch( Angle( math.Rand(-0.5,0.5), math.Rand(-0.2,0.2), math.Rand(-0.2,0.2) ) )
			end
		end

		if self.Owner:KeyDown(IN_JUMP) then
			if self.JumpKick == 0 then
				self.JumpKick = 1
				self.Owner:ViewPunch( Angle( math.Rand(-15,15), math.Rand(-15,15), math.Rand(-3,3) ) )
				timer.Create( "wallop", 0.1, 3, function()
				self.Owner:ViewPunch( Angle( math.Rand(-15,15), math.Rand(-15,15), math.Rand(-3,3) ) )
				end)
			end
		end
		
		local cmd = self.Owner:GetCurrentCommand()
		
		self.LastThink = self.LastThink or 0
		local fDelta = (CurTime() - self.LastThink)
		self.LastThink = CurTime()

		if self.Owner:OnGround() then
			if !self.Owner:KeyDown(IN_JUMP) then
				self.JumpKick = 0
			end
			else
			self.Owner:ViewPunch( Angle( math.Rand(-0.5,0.5), math.Rand(-0.2,0.2), math.Rand(-0.2,0.2) ) )
			end
		end
end

function SWEP:HUDShouldDraw( name )
	if (name == "CHudHealth") then return false end
	if (name == "CHudSuitPower") then return false end
	if (name == "CHudBattery") then return false end
	if (name == "CHudCrosshair") then return false end
	if (name == "CHudAmmo") then return false end
	if (name == "CHudSecondaryAmmo") then return false end
	if (name == "CHudChat") then return false end
	if (name == "CHudDeathNotice") then return false end
	if (name == "CHudTrain") then return false end
	if (name == "CHudMessage") then return false end
	if (name == "CHudVoiceStatus") then return false end
	if (name == "CHudVoiceSelfStatus") then return false end
	if (name == "CHudDamageIndicator") then return false end
	if (name == "CAchievementNotificationPanel") then return false end
	if (name == "CHudSquadStatus") then return false end
	if (name == "CTargetID") then return false end
	return true
end

function SWEP:AdjustMouseSensitivity()
	-- if(ZoomLevel == 0) then return 1 end
	return self.Owner:GetFOV() / 75
end

function SWEP:Holster()
	if self.Owner:IsPlayer() then
		self.Owner:SetFOV( 0, 0.15 )
		self.Owner:SetCanZoom( true )
		ZoomLevel = 0
	end
	return true
end

function SWEP:OnRemove()
	if self.Owner:IsPlayer() then
		self.Owner:SetFOV( 0, 0.15 )
		self.Owner:SetCanZoom( true )
		ZoomLevel = 0
	end
	return true
end