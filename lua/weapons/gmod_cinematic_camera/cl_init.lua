include('shared.lua')

SWEP.PrintName			= "Cinematic Camera"
SWEP.Slot				= 5
SWEP.SlotPos			= 1
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= false
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.WepSelectIcon		= surface.GetTextureID( "vgui/entities/cinematic_camera" )

local cc_letterbox = surface.GetTextureID ( "overlays/cc_letterbox")
local cc_grain = surface.GetTextureID ( "overlays/cc_grain")
local camcorder_noise = surface.GetTextureID ( "overlays/camcorder_noise")
local camcorder_visor1 = surface.GetTextureID ( "overlays/camcorder_visor1")
local camcorder_visor2 = surface.GetTextureID ( "overlays/camcorder_visor2")
local camcorder_rec = surface.GetTextureID ( "overlays/camcorder_rec")
local camcorder_hd = surface.GetTextureID ( "overlays/camcorder_hd")
local camcorder_battery = surface.GetTextureID ( "overlays/camcorder_battery")
local vignette = surface.GetTextureID ( "overlays/cc_vignette")

function SWEP:DrawHUD()
	if GetConVar("gmod_cinematic_camera_color_correction"):GetBool() == true then
		-- Cold Climatica
		local cc_a = {
		[ "$pp_colour_addr" ] = 0.0,
		[ "$pp_colour_addg" ] = 0.117,
		[ "$pp_colour_addb" ] = 0.196,
		[ "$pp_colour_brightness" ] = -0.25,
		[ "$pp_colour_contrast" ] = 1.40,
		[ "$pp_colour_colour" ] = 0.85,
		[ "$pp_colour_mulr" ] = 0.0,
		[ "$pp_colour_mulg" ] = 0.0,
		[ "$pp_colour_mulb" ] = 0.0
		}
		-- Contrasta
		local cc_b = {
		[ "$pp_colour_addr" ] = 0.235,
		[ "$pp_colour_addg" ] = 0.117,
		[ "$pp_colour_addb" ] = 0.156,
		[ "$pp_colour_brightness" ] = -0.19,
		[ "$pp_colour_contrast" ] = 1.15,
		[ "$pp_colour_colour" ] = 0.95,
		[ "$pp_colour_mulr" ] = 0.0,
		[ "$pp_colour_mulg" ] = 0.0,
		[ "$pp_colour_mulb" ] = 0.0
		}
		-- Greenity
		local cc_c = {
		[ "$pp_colour_addr" ] = 0.0,
		[ "$pp_colour_addg" ] = 0.117,
		[ "$pp_colour_addb" ] = 0.0,
		[ "$pp_colour_brightness" ] = -0.12,
		[ "$pp_colour_contrast" ] = 1.00,
		[ "$pp_colour_colour" ] = 0.80,
		[ "$pp_colour_mulr" ] = 0.0,
		[ "$pp_colour_mulg" ] = 0.0,
		[ "$pp_colour_mulb" ] = 0.0
		}
		-- Warmer Bros
		local cc_d = {
		[ "$pp_colour_addr" ] = 0.352,
		[ "$pp_colour_addg" ] = 0.235,
		[ "$pp_colour_addb" ] = 0.117,
		[ "$pp_colour_brightness" ] = -0.15,
		[ "$pp_colour_contrast" ] = 1.10,
		[ "$pp_colour_colour" ] = 1.10,
		[ "$pp_colour_mulr" ] = 0.0,
		[ "$pp_colour_mulg" ] = 0.0,
		[ "$pp_colour_mulb" ] = 0.0
		}
		-- Sadness
		local cc_e = {
		[ "$pp_colour_addr" ] = 0.0,
		[ "$pp_colour_addg" ] = 0.039,
		[ "$pp_colour_addb" ] = 0.196,
		[ "$pp_colour_brightness" ] = -0.23,
		[ "$pp_colour_contrast" ] = 1.50,
		[ "$pp_colour_colour" ] = 0.0,
		[ "$pp_colour_mulr" ] = 0.0,
		[ "$pp_colour_mulg" ] = 0.0,
		[ "$pp_colour_mulb" ] = 0.0
		}
		-- Darka
		local cc_f = {
		[ "$pp_colour_addr" ] = 0.275,
		[ "$pp_colour_addg" ] = 0.352,
		[ "$pp_colour_addb" ] = 0.313,
		[ "$pp_colour_brightness" ] = -0.45,
		[ "$pp_colour_contrast" ] = 1.35,
		[ "$pp_colour_colour" ] = 0.50,
		[ "$pp_colour_mulr" ] = 0.0,
		[ "$pp_colour_mulg" ] = 0.0,
		[ "$pp_colour_mulb" ] = 0.0
		}
		-- Blockbuster
		local cc_g = {
		[ "$pp_colour_addr" ] = 0.0392,
		[ "$pp_colour_addg" ] = 0.0392,
		[ "$pp_colour_addb" ] = 0.196,
		[ "$pp_colour_brightness" ] = -0.15,
		[ "$pp_colour_contrast" ] = 1.50,
		[ "$pp_colour_colour" ] = 0.80,
		[ "$pp_colour_mulr" ] = 0.0,
		[ "$pp_colour_mulg" ] = 0.0,
		[ "$pp_colour_mulb" ] = 0.0
		}
		-- Field Of Battle
		local cc_h = {
		[ "$pp_colour_addr" ] = 0.0,
		[ "$pp_colour_addg" ] = 0.078,
		[ "$pp_colour_addb" ] = 0.117,
		[ "$pp_colour_brightness" ] = -0.12,
		[ "$pp_colour_contrast" ] = 1.75,
		[ "$pp_colour_colour" ] = 0.50,
		[ "$pp_colour_mulr" ] = 0.0,
		[ "$pp_colour_mulg" ] = 0.196,
		[ "$pp_colour_mulb" ] = 0.784
		}
		-- Summer
		local cc_i = {
		[ "$pp_colour_addr" ] = 0.125,
		[ "$pp_colour_addg" ] = 0.0,
		[ "$pp_colour_addb" ] = 0.0,
		[ "$pp_colour_brightness" ] = -0.05,
		[ "$pp_colour_contrast" ] = 1.10,
		[ "$pp_colour_colour" ] = 1.25,
		[ "$pp_colour_mulr" ] = 0.078,
		[ "$pp_colour_mulg" ] = 0.039,
		[ "$pp_colour_mulb" ] = 0.0
		}
		-- Calm
		local cc_j = {
		[ "$pp_colour_addr" ] = 0.136,
		[ "$pp_colour_addg" ] = 0.039,
		[ "$pp_colour_addb" ] = 0.039,
		[ "$pp_colour_brightness" ] = -0.10,
		[ "$pp_colour_contrast" ] = 1.35,
		[ "$pp_colour_colour" ] = 0.85,
		[ "$pp_colour_mulr" ] = 0.0,
		[ "$pp_colour_mulg" ] = 0.0,
		[ "$pp_colour_mulb" ] = 0.0
		}
		-- Rainmaker
		local cc_k = {
		[ "$pp_colour_addr" ] = 0.0313,
		[ "$pp_colour_addg" ] = 0.0313,
		[ "$pp_colour_addb" ] = 0.039,
		[ "$pp_colour_brightness" ] = -0.05,
		[ "$pp_colour_contrast" ] = 0.80,
		[ "$pp_colour_colour" ] = 0.75,
		[ "$pp_colour_mulr" ] = 0.0,
		[ "$pp_colour_mulg" ] = 0.0,
		[ "$pp_colour_mulb" ] = 0.0
		}
		
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "a" then
			DrawColorModify( cc_a )
		end
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "b" then
			DrawColorModify( cc_b )
		end
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "c" then
			DrawColorModify( cc_c )
		end
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "d" then
			DrawColorModify( cc_d )
		end
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "e" then
			DrawColorModify( cc_e )
		end
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "f" then
			DrawColorModify( cc_f )
		end
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "g" then
			DrawColorModify( cc_g )
		end
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "h" then
			DrawColorModify( cc_h )
		end
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "i" then
			DrawColorModify( cc_i )
		end
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "j" then
			DrawColorModify( cc_j )
		end
		if GetConVar("gmod_cinematic_camera_color_correction_style"):GetString() == "k" then
			DrawColorModify( cc_k )
		end
	end
	
	if GetConVar("gmod_cinematic_camera_vignette"):GetBool() == true then
	local w,h = ScrW(),ScrH()
		surface.SetDrawColor ( 255, 255, 255, 255 )
		surface.SetTexture ( vignette )
		surface.DrawTexturedRect ( 0,0, w, h )
	end

	if GetConVar("gmod_cinematic_camera_grain"):GetBool() == true then
		local w,h = ScrW(),ScrH()
		surface.SetDrawColor ( 255, 255, 255, 255 )
		surface.SetTexture ( cc_grain )
		surface.DrawTexturedRect ( 0,0, w, h )
	end
	
	if GetConVar("gmod_cinematic_camera_noise"):GetBool() == true then
		local w,h = ScrW(),ScrH()
		surface.SetDrawColor ( 255, 255, 255, 255 )
		surface.SetTexture ( camcorder_noise )
		surface.DrawTexturedRect ( 0,0, w, h )
	end

	if GetConVar("gmod_cinematic_camera_overlay"):GetBool() == true then
		local w,h = ScrW(),ScrH()

		surface.SetDrawColor ( 255, 255, 255, 255 )
		surface.SetTexture ( camcorder_visor1 )
		surface.DrawTexturedRect ( w / 2 - 512, h / 2 - 256, 1024, 512 )
		
		surface.SetDrawColor ( 255, 255, 255, 255 )
		surface.SetTexture ( camcorder_visor2 )
		surface.DrawTexturedRect ( 0, 0, w, h )
		
		surface.SetDrawColor ( 255, 255, 255, 255 )
		surface.SetTexture ( camcorder_rec )
		surface.DrawTexturedRect ( w * 0.84, h * 0.14, 128, 64 )
		
		surface.SetDrawColor ( 255, 255, 255, 255 )
		surface.SetTexture ( camcorder_hd )
		surface.DrawTexturedRect ( w * 0.08, h * 0.79, 128, 128 )
			
		surface.SetDrawColor ( 255, 255, 255, 255 )
		surface.SetTexture ( camcorder_battery )
		surface.DrawTexturedRect ( w * 0.08, h * 0.14, 128, 64 )
	end
	
	if GetConVar("gmod_cinematic_camera_letterbox"):GetBool() == true then
		local w,h = ScrW(),ScrH()
		surface.SetDrawColor ( 255, 255, 255, 255 )
		surface.SetTexture ( cc_letterbox )
		surface.DrawTexturedRect ( 0,0, w, h )
	end
end